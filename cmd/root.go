package cmd

import (
	"github.com/spf13/cobra"
)

var (
	rootCmd = &cobra.Command{
		Use:   "responsimg",
		Short: "A utility for resizing images into common responsive breakpoints",
		Long:  `A powerful utility for easily resizing images into responsive breakpoints with support for globbing and auto orientation`,
	}
)

func Execute() error {
	return rootCmd.Execute()
}

func init() {
	rootCmd.AddCommand(resizeCmd)
}
