package cmd

import (
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/vorpalhex/responsimg/lib"
)

var (
	resizeCmd = &cobra.Command{
		Use:   "resize",
		Args:  cobra.MinimumNArgs(1),
		Short: "resize a set of images",
		Long:  `resize images passed as an array with glob support`,
		Run: func(cmd *cobra.Command, args []string) {
			inputImages := lib.ResolveGlobs(args)

			for _, width := range viper.GetIntSlice("widths") {
				for _, inputPath := range inputImages {
					outputPath := lib.GetOutputPath(inputPath, width, viper.GetString("outputDir"))
					lib.ResizeImg(inputPath, width, !viper.GetBool("disableAutoOrientation"), outputPath)
				}
			}
		},
	}
	widths                 []int
	disableAutoOrientation bool
	outputDir              string
)

func init() {
	resizeCmd.Flags().IntSliceP("widths", "w", widths, "supply an array of widths to resize to")
	viper.BindPFlag("widths", resizeCmd.Flags().Lookup("widths"))
	viper.SetDefault("widths", []int{480, 720, 1080, 2160})

	resizeCmd.Flags().Bool("disableAutoOrientation", disableAutoOrientation, "disable automatically orienting images")
	viper.BindPFlag("disableAutoOrientation", resizeCmd.Flags().Lookup("disableAutoOrientation"))
	viper.SetDefault("disableAutoOrientation", false)

	resizeCmd.Flags().StringP("outputDir", "o", outputDir, "where to save output images, defaults to current directory")
	viper.BindPFlag("outputDir", resizeCmd.Flags().Lookup("outputDir"))
	cwd, _ := os.Getwd()
	viper.SetDefault("outputDir", cwd)
}
