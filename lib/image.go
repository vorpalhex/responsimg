package lib

import (
	"log"

	"github.com/disintegration/imaging"
)

func ResizeImg(inputPath string, width int, autoOrient bool, outPath string) error {
	src, err := imaging.Open(inputPath, imaging.AutoOrientation(autoOrient))

	if err != nil {
		log.Printf("Failed to open image %s", inputPath)
		return err
	}

	resized := imaging.Resize(src, width, 0, imaging.Lanczos)
	err = imaging.Save(resized, outPath)

	if err != nil {
		log.Printf("Failed to save image %s", outPath)
		return err
	}

	return nil

}
