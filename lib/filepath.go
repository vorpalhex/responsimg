package lib

import (
	"log"
	"path/filepath"
	"strconv"
	"strings"
)

func ResolveGlobs(patterns []string) []string {
	var result []string

	for _, pattern := range patterns {
		matches, err := filepath.Glob(pattern)
		result = append(result, matches...)

		if err != nil {
			log.Print(err)
		}
	}

	for idx, relPath := range result {
		absPath, err := filepath.Abs(relPath)

		if err != nil {
			log.Print(err)
		} else {
			result[idx] = absPath
		}
	}

	return result
}

func GetOutputPath(inputPath string, width int, outputDir string) string {
	_, filename := filepath.Split(inputPath)
	ext := filepath.Ext(filename)

	newName := strings.Replace(filename, ext, "-"+strconv.Itoa(width)+"w"+ext, 1)

	output := filepath.Join(outputDir, newName)

	return output
}
