package main

import (
	"gitlab.com/vorpalhex/responsimg/cmd"
)

func main() {
	cmd.Execute()
}
