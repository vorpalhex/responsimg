# responseimg

A utility for quickly bulk resizing images at responsive breakpoints with proper orientation. Default output widths are 480px, 720px, 1080px and 2160px but can be changed easily. 

## Basic Usage

`responseimg resize -o my/output/dir image1.jpg` will result in `my/output/dir` containing `image1-480w.jpg`, `image1-720w.jpg`, `image1-1080w.jpg`, and `image1-2160w.jpg`. 

`responseimg resize -o my/output/dir -w 320 -w 240 image1.jpg` will result in `my/output/dir` containing `image1-240w.jpg` and `image1-320w.jpg`.

`responseimg resize -o my/output/dir -w 320  image*` in a directory with files `image1.jpg` and `image2.jpg` will result in `my/output/dir` containing `image1-320w.jpg` and `image2-320w.jpg`. 

If you need to disable auto orientation, you can do: `responseimg resize --disableAutoOrientation image1.jpg`

You can pass several positional args, including multiple globs, and we'll process them.

## Manual

```
Usage:
  responseimg resize [flags] [paths]

Flags:
      --disableAutoOrientation   disable automatically orienting images
  -h, --help                     help for resize
  -o, --outputDir string         where to save output images, defaults to current directory
  -w, --widths ints              supply an array of widths to resize to
```

## Contributing

Contributions are welcome, please open a PR using a fork. I'm currently kicking out manual builds just for MacOS but I can add CI support if there's demand for it. Likewise I am happy to provide builds for other platforms as requested, please simply open an issue.

## License

Copyright (C) 2020 Vorpalhex

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.